package me.koledogcodes.serversigns.utili;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.meta.ItemEnchantment;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.Item;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.item.Enchantment;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.property.InventoryDimension;
import org.spongepowered.api.item.inventory.property.InventoryTitle;
import org.spongepowered.api.service.economy.Currency;
import org.spongepowered.api.service.economy.EconomyService;
import org.spongepowered.api.service.economy.account.UniqueAccount;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.serializer.TextSerializers;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import com.pixelmonmod.pixelmon.config.PixelmonEntityList;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.storage.PixelmonStorage;
import com.pixelmonmod.pixelmon.storage.PlayerStorage;

import me.koledogcodes.serversigns.ServerSigns;
import me.koledogcodes.serversigns.configs.FileConfiguration;
import me.koledogcodes.serversigns.configs.JSONConfig;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;

public class SignUtili {

	public SignUtili() {
		
	}
	
	final static String USER_PATH = "config/ServerSigns/users";
	final static String SIGN_PATH = "config/ServerSigns/signs";
	private static HashMap<UUID, SignAction> playerSignAction = new HashMap<UUID, SignAction>();
	private static HashMap<UUID, String> playerSignData = new HashMap<UUID, String>();
	private static HashMap<UUID, Integer> playerSignSubData = new HashMap<UUID, Integer>();
	private static String validBlocks = null;
	private static long YEAR = 1000 * 60 * 60 *24 * 365;
	private static long DAY = 1000 * 60 * 60 *24;
	private static long HOUR = 1000 * 60 * 60;
	private static long MIN = 1000 * 60;
	private static long SECS = 1000;
	private static EconomyService economyService;
	public static final List<UUID> signSpyUsers = new ArrayList<UUID>();
	
	public static boolean playerFileExists(UUID uuid){
		File f = new File(USER_PATH, uuid.toString());
		
		return f.exists();
	}
	
	public static boolean signFileExists(World world, Location<?> loc){
		File f = new File(SIGN_PATH, parseLocationToString(world, loc) + ".json");
		
		return f.exists();
	}
	
	public static String parseLocationToString(World world, Location<?> loc){
		return world.getName() + "@" + loc.getBlockX() + "@" + loc.getBlockY() + "@" + loc.getBlockZ();
	}
	
	public static Location<?> parseStringToLocation(String loc){
		World w = Sponge.getServer().getWorld(loc.split("@")[0]).get();
		return w.getLocation(Integer.valueOf(loc.split("@")[1]), Integer.valueOf(loc.split("@")[2]), Integer.valueOf(loc.split("@")[3]));
	}
	
	public static void generatePlayerFile(UUID uuid){
		if (!playerFileExists(uuid)){
			JSONConfig config = new JSONConfig(USER_PATH, uuid.toString());
			config.setValue("created_at", System.currentTimeMillis());
		}
	}
	
	public static void generateSign(World world, Location<?> loc){
		if (!signFileExists(world, loc)){
			JSONConfig config = new JSONConfig(SIGN_PATH, parseLocationToString(world, loc));
			config.generateValue("created_at", System.currentTimeMillis());
			config.generateValue("commands", new Object[]{});
			config.generateValue("price", 0);
			config.generateValue("exp-price", 0);
			config.generateValue("permission", "svs.use");
			config.generateValue("uses", -1);
			config.generateValue("cooldown", 0);
			config.generateValue("global-cooldown", 0);
			config.generateValue("give-items", "");
			config.generateValue("trade-items", "");
			config.generateValue("trade-pokemon", "");
		}
	}
	
	public static void deleteSign(World world, Location<?> loc){
		if (signFileExists(world, loc)){
			JSONConfig config = new JSONConfig(SIGN_PATH, parseLocationToString(world, loc));
			config.deleteConfig();
		}
	}
	
	public static boolean checkPlayerAction(UUID uuid){
		return playerSignAction.containsKey(uuid);
	}
	
	public static boolean checkPlayerData(UUID uuid){
		return playerSignData.containsKey(uuid);
	}
	
	public static boolean checkPlayerSubData(UUID uuid){
		return playerSignSubData.containsKey(uuid);
	}
	
	public static void setPlayerAction(UUID uuid, SignAction action){
		playerSignAction.put(uuid, action);
	}
	
	public static void setPlayerData(UUID uuid, String data){
		playerSignData.put(uuid, data);
	}
	
	public static void setPlayerSubData(UUID uuid, int data){
		playerSignSubData.put(uuid, data);
	}
	
	public static SignAction getPlayerAction(UUID uuid){
		return playerSignAction.get(uuid);
	}
	
	public static String getPlayerData(UUID uuid){
		return playerSignData.get(uuid);
	}
	
	public static int getPlayerSubData(UUID uuid){
		return playerSignSubData.get(uuid);
	}
	
	public static void clearPlayerActionData(UUID uuid){
		if (checkPlayerAction(uuid)){ playerSignAction.remove(uuid); }
		if (checkPlayerData(uuid)){ playerSignData.remove(uuid); }
		if (checkPlayerSubData(uuid)){ playerSignSubData.remove(uuid); }
	}
	
	public static boolean validateBlock(BlockType type){
		if (validBlocks == null){
			validBlocks = FileConfiguration.getMainConfig().getString("accepted-blocks");
		}
		
		return validBlocks.contains(type.getName().toLowerCase().replace("minecraft:", ""));

	}
	
	public static boolean validateEntity(Entity type){
		if (validBlocks == null){
			validBlocks = FileConfiguration.getMainConfig().getString("accepted-blocks");
		}
		
		return validBlocks.contains(type.getType().getName().toLowerCase());

	}
	
	public static String checkEntity(Entity type){
		return type.getType().getName();

	}
	
	public static boolean checkItem(Player player, ItemStack itemstack){
		Inventory items = player.getInventory().query(itemstack);
	 	
		if (items.peek(itemstack.getQuantity()).filter(stack -> stack.getQuantity() >= itemstack.getQuantity()).isPresent()) {
			return true;
		} 
		else {
			return false;
		}
	}
	
	public static boolean checkPokemon(Player player, JSONObject obj, int slot){
		try {
				EntityPixelmon pokemon = getPlayerPokemon(player, slot);
				
				if (pokemon == null){ return false; }
				
				List<Boolean> checkList = new ArrayList<Boolean>();
				
				if (obj.has("lvl")){
					if (obj.getInt("lvl") == pokemon.getLvl().getLevel()){ checkList.add(true); }
					else { checkList.add(false); }
				}
				if (obj.has("type")){
					if (obj.getString("type").equalsIgnoreCase(pokemon.getPokemonName())){ checkList.add(true); }
					else { checkList.add(false); }
				}
				if (obj.has("name")){
					if (obj.getString("name").equalsIgnoreCase(pokemon.getNickname())){ checkList.add(true); }
					else { checkList.add(false); }
				}
				if (obj.has("nature")){
					if (obj.getString("nature").equalsIgnoreCase(pokemon.getNature().name())){ checkList.add(true); }
					else { checkList.add(false); }
				}
				if (obj.has("ability")){
					if (obj.getString("ability").equalsIgnoreCase(pokemon.getAbility().getName())){ checkList.add(true); }
					else { checkList.add(false); }
				}
				if (obj.has("growth")){
					if (obj.getString("growth").equalsIgnoreCase(pokemon.getGrowth().name())){ checkList.add(true); }
					else { checkList.add(false); }
				}
				if (obj.has("gender")){
					if (obj.getString("gender").equalsIgnoreCase(pokemon.gender.name())){ checkList.add(true); }
					else { checkList.add(false); }
				}
				if (obj.has("shiny")){
					if (obj.getBoolean("shiny") == pokemon.getIsShiny()){ checkList.add(true); }
					else { checkList.add(false); }
				}
				if (obj.has("moveset")){
					JSONObject moves = obj.getJSONObject("moveset");
					
					List<String> pokemonMoveset = Arrays.asList(pokemon.getMoveset().get(0) != null ? pokemon.getMoveset().get(0).baseAttack.getLocalizedName() : "Empty",pokemon.getMoveset().get(1) != null ? pokemon.getMoveset().get(1).baseAttack.getLocalizedName() : "Empty", pokemon.getMoveset().get(2) != null ? pokemon.getMoveset().get(2).baseAttack.getLocalizedName() : "Empty", pokemon.getMoveset().get(3) != null ? pokemon.getMoveset().get(3).baseAttack.getLocalizedName() : "Empty");
					List<String> wantedMoveset = new ArrayList<String>();
					
					for (int z = 1; z < 5; z++){
						String move = moves.getString(String.valueOf(z));
						wantedMoveset.add(move);
					}
					
					Collections.sort(pokemonMoveset);
					Collections.sort(wantedMoveset);
					
					if (pokemonMoveset.equals(wantedMoveset)){ checkList.add(true); }
					else { checkList.add(false); }
				}
				
				if (checkList.contains(false)){ return false; } else { return true; }
		}
		catch (Exception e){
			return false;
		}
	}
	
	public static boolean checkPokemon(Player player, JSONObject obj){
		try {
			
			for (int i = 0; i < getPartySize(player); i++){
				EntityPixelmon pokemon = getPlayerPokemon(player, i);
				
				if (pokemon == null){ continue; }
				
				List<Boolean> checkList = new ArrayList<Boolean>();
				
				if (obj.has("lvl")){
					if (obj.getInt("lvl") == pokemon.getLvl().getLevel()){ checkList.add(true); }
					else { checkList.add(false); }
				}
				if (obj.has("type")){
					if (obj.getString("type").equalsIgnoreCase(pokemon.getPokemonName())){ checkList.add(true); }
					else { checkList.add(false); }
				}
				if (obj.has("name")){
					if (obj.getString("name").equalsIgnoreCase(pokemon.getNickname())){ checkList.add(true); }
					else { checkList.add(false); }
				}
				if (obj.has("nature")){
					if (obj.getString("nature").equalsIgnoreCase(pokemon.getNature().name())){ checkList.add(true); }
					else { checkList.add(false); }
				}
				if (obj.has("ability")){
					if (obj.getString("ability").equalsIgnoreCase(pokemon.getAbility().getName())){ checkList.add(true); }
					else { checkList.add(false); }
				}
				if (obj.has("growth")){
					if (obj.getString("growth").equalsIgnoreCase(pokemon.getGrowth().name())){ checkList.add(true); }
					else { checkList.add(false); }
				}
				if (obj.has("gender")){
					if (obj.getString("gender").equalsIgnoreCase(pokemon.gender.name())){ checkList.add(true); }
					else { checkList.add(false); }
				}
				if (obj.has("shiny")){
					if (obj.getBoolean("shiny") == pokemon.getIsShiny()){ checkList.add(true); }
					else { checkList.add(false); }
				}
				if (obj.has("moveset")){
					JSONObject moves = obj.getJSONObject("moveset");
					
					List<String> pokemonMoveset = Arrays.asList(pokemon.getMoveset().get(0) != null ? pokemon.getMoveset().get(0).baseAttack.getLocalizedName() : "Empty",pokemon.getMoveset().get(1) != null ? pokemon.getMoveset().get(1).baseAttack.getLocalizedName() : "Empty", pokemon.getMoveset().get(2) != null ? pokemon.getMoveset().get(2).baseAttack.getLocalizedName() : "Empty", pokemon.getMoveset().get(3) != null ? pokemon.getMoveset().get(3).baseAttack.getLocalizedName() : "Empty");
					List<String> wantedMoveset = new ArrayList<String>();
					
					for (int z = 1; z < 5; z++){
						String move = moves.getString(String.valueOf(z));
						wantedMoveset.add(move);
					}
					
					Collections.sort(pokemonMoveset);
					Collections.sort(wantedMoveset);
					
					if (pokemonMoveset.equals(wantedMoveset)){ checkList.add(true); }
					else { checkList.add(false); }
				}
				
				if (checkList.contains(false)){ continue; } else { return true; }
			}
			
			return false;
		}
		catch (Exception e){
			return false;
		}
	}
	
	public static void removePokemon(Player player, JSONObject obj){
		try {
			for (int i = 0; i < getPartySize(player); i++){
				EntityPixelmon pokemon = getPlayerPokemon(player, i);
				
				if (pokemon == null){ continue; }
				
				if (!checkPokemon(player, obj, i)){ continue; }
				
				PlayerStorage ps = PixelmonStorage.pokeBallManager.getPlayerStorageFromUUID((MinecraftServer) Sponge.getServer(), player.getUniqueId()).get();
				ps.removeFromPartyPlayer(i);
				break;
			}
		}
		catch (Exception e){
			return;
		}
	}
	
	public static void removeItem(Player player, ItemStack itemstack){
		 Inventory items = player.getInventory().query(itemstack);
		 	
		if (items.peek(itemstack.getQuantity()).filter(stack -> stack.getQuantity() >= itemstack.getQuantity()).isPresent()) {
    		items.poll(itemstack.getQuantity());
		} 
		else {
    		ChatUtili.sendPrefixedMessage(player, "&cYou do not have enough item.");
		}
	}
	
	public static JSONConfig getSignData(World world, Location<?> loc){
		generateSign(world, loc);
		
		return new JSONConfig(SIGN_PATH, parseLocationToString(world, loc));
	}
	
	public static JSONConfig getSignData(String loc){
		return new JSONConfig(SIGN_PATH, loc);
	}
	
	public static JSONConfig getUserData(UUID uuid){
		return new JSONConfig(USER_PATH, uuid.toString());
	}
	
	public static String getTime(long time){
		double y=0,d=0,h=0,m=0,s=0;
		
		if (time >= YEAR){
			y = time / YEAR;
			time -= y * YEAR;
		}
		
		if (time >= DAY){
			d = time / DAY;
			time -= d * DAY;
		}
		
		if (time >= HOUR){
			h = time / HOUR;
			time -= h * HOUR;
		}
		
		if (time >= MIN){
			m = time / MIN;
			time -= m * MIN;
		}
		
		if (time >= SECS){
			s = time / SECS;
			time -= s * SECS;
		}
		
		String result = "";
		
		if (y > 0){
			result += Math.round(y) + "y ";
			
			if (d > 0){
				result += Math.round(d) + "d ";
			}
			
			if (h > 0){
				result += Math.round(h) + "h ";
			}
			
			if (m > 0){
				result += Math.round(m) + "m ";
			}
			
			if (s > 0){
				result += Math.round(s) + "s";
			}
		}
		else if (d > 0){
			result += Math.round(d) + "d ";
			
			if (h > 0){
				result += Math.round(h) + "h ";
			}
			
			if (m > 0){
				result += Math.round(m) + "m ";
			}
			
			if (s > 0){
				result += Math.round(s) + "s";
			}
		}
		else if (h > 0){
			result += Math.round(h) + "h ";
			
			if (m > 0){
				result += Math.round(m) + "m ";
			}
			
			if (s > 0){
				result += Math.round(s) + "s";
			}
		}
		else if (m > 0){
			result += Math.round(m) + "m ";
			
			if (s > 0){
				result += Math.round(s) + "s";
			}
		}
		else if (s > 0){
			result += Math.round(s) + "s";
		}
		else {
			result = "N/A";
		}
		
		return result;
	}
	
	public static void setupEconomy(){
		Optional<EconomyService> serviceOpt = Sponge.getServiceManager().provide(EconomyService.class);
		
		if (!serviceOpt.isPresent()) {
			System.out.println("[ServerSigns] No economy plugin found...");
			return;
		}
		
		economyService = serviceOpt.get();
	}
	
	public static double getBalance(UUID uuid){
		if (economyService == null){
			return -1;
		}
		
		Optional<UniqueAccount> uOpt = economyService.getOrCreateAccount(uuid);
		
		if (uOpt.isPresent()) {
		    UniqueAccount acc = uOpt.get();
		    double balance = acc.getBalance(economyService.getDefaultCurrency()).doubleValue();
		    return balance;
		}
		else {
			return -1;
		}
	}
	
	public static UniqueAccount getAccount(UUID uuid){
		setupEconomy();
		
		if (economyService == null){
			return null;
		}
		
		Optional<UniqueAccount> uOpt = economyService.getOrCreateAccount(uuid);
		
		if (uOpt.isPresent()) {
		    UniqueAccount acc = uOpt.get();
		    return acc;
		}
		else {
			return null;
		}
	}
	
	public static Currency getDefaultCurrency(){
		return economyService.getDefaultCurrency();
	}
	
	public static boolean economyExists(){
		return (economyService != null);
	}
	
	public static void printSignInformation(Player source, World world, Location<?> loc){
		if (signFileExists(world, loc)){
			JSONConfig config = new JSONConfig(SIGN_PATH, parseLocationToString(world, loc));
			Object[] actions = config.getArray("commands");
			
			ChatUtili.sendMessage(source, "&f----- &cSign Information &f-----");
			for (int i = 0; i < actions.length; i++){
				ChatUtili.sendMessage(source, "&9" + i + ". &a" + actions[i]);
			}
				ChatUtili.sendMessage(source, " ");
				ChatUtili.sendMessage(source, "&9Permission: &a" + config.getObject("permission"));
				ChatUtili.sendMessage(source, "&9Price: &a" + config.getObject("price"));
				//ChatUtili.sendMessage(source, "&9Exp Price: &a" + config.getObject("exp-price") + "Levels");
				ChatUtili.sendMessage(source, "&9Cooldown: &a" + getTime(config.getLong("cooldown")));
				ChatUtili.sendMessage(source, "&9Global Cooldown: &a" + getTime(config.getLong("global-cooldown")));
				ChatUtili.sendMessage(source, "&9Number of Uses: &a" + config.getObject("uses"));
				ChatUtili.sendMessage(source, "&9Number of Per Player Uses: &a" + config.getObject("player-uses"));
		
				if (config.hasKey("trade-pokemon")){
					if (config.getObject("trade-pokemon") != ""){
						ChatUtili.sendMessage(source, "&f----- &dPokemon Trading Information &f-----");
						JSONArray array = config.getJSONArray("trade-pokemon");
						
						for (int i = 0; i < array.length(); i++){
							JSONObject obj;
							try {
								obj = array.getJSONObject(i);
							} 
							catch (JSONException e) {
								continue;
							}
							
							if (obj == null){ continue; }
							
							ChatUtili.sendPrefixedPokemonHoverMessage(source, "&cPokemon #" + (i + 1), obj);
						}
					}
				}

		}
		else {
			ChatUtili.sendPrefixedMessage(source, "&cThat is not a serversign sign.");
		}
	}
	
	public static Inventory createInventory(String title){
		return Inventory.builder().property(
		"title", new InventoryTitle(Text.of(title))).property(
		"inventorydimension", new InventoryDimension(9, 3)).
		build(ServerSigns.class);
	}
	
	public static void loadTradeItemsInv(Player player, JSONConfig config){
		/*Inventory inv = createInventory("Chest");
		
		try {
			List<ItemStack> items = SignUtili.deserializeItemStackArray(config.getJSONArray("trade-items"));
		
			for (ItemStack i: items){
				if (i == null){ continue; }
				
				inv.offer(i);
			}
		}
		catch (Exception exc){
			exc.printStackTrace();
		}
		
		player.openInventory(inv, Cause.builder().owner(player).build());
		*/
	}
	
	public static JSONArray serializeItemStackArray(List<ItemStack> items){
		JSONArray jsonArray =  new JSONArray();
		
		for (int i= 0; i < items.size(); i++){
			JSONObject obj = serializeItemStack(String.valueOf(i), items.get(i)).orElse(new JSONObject());
		
			jsonArray.put(obj);
		}
		
		return jsonArray;
	}
	
	public static List<ItemStack> deserializeItemStackArray(JSONArray array){
		List<ItemStack> items = new ArrayList<ItemStack>();
		
		try {
			for (int i = 0; i < array.length(); i++){
				if (array.get(i) == null){ continue; }
				
				JSONObject obj = array.getJSONObject(i).getJSONObject(String.valueOf(i));
				items.add(deserializeItemStack(obj));
			}
			
			return items;
		}
		catch (Exception e){
			items.add(null);
		}
		
		return items;
	}
	
	private static Optional<JSONObject> serializeItemStack(String key, ItemStack item) {
		Optional<JSONObject> outer = Optional.of(new JSONObject());
		
		try {
			JSONObject obj = new JSONObject();
			obj.put("id", item.getItem().getType().getName());
			obj.put("amount", item.getQuantity());
			
			if (item.getValue(Keys.DISPLAY_NAME).isPresent()){
				obj.put("display_name", TextSerializers.JSON.serialize(item.getValue(Keys.DISPLAY_NAME).get().get()));
			}
			
			if (item.getValue(Keys.ITEM_LORE).isPresent()){
				List<String> lores = new ArrayList<String>();
				
				for (Text t: item.getValue(Keys.ITEM_LORE).get().get()){
					lores.add(TextSerializers.JSON.serialize(t));
				}
				
				obj.put("lore", lores);
			}
			
			if (item.getValue(Keys.GLOWING).isPresent()){
				obj.put("glowing", item.getValue(Keys.GLOWING).get().get());
			}
			
			if (item.getValue(Keys.ITEM_DURABILITY).isPresent()){
				obj.put("durability", item.getValue(Keys.ITEM_DURABILITY).get().get());
			}
			
			if (item.getValue(Keys.DYE_COLOR).isPresent()){
				obj.put("dye_color", item.getValue(Keys.DYE_COLOR).get().get().getId());
			}
		
			if (item.getValue(Keys.STORED_ENCHANTMENTS).isPresent()){
				String name = item.getValue(Keys.STORED_ENCHANTMENTS).get().get(0).getEnchantment().getId().toUpperCase();
				int level = item.getValue(Keys.STORED_ENCHANTMENTS).get().get().get(0).getLevel();
				obj.put("book_enchant", name + "#" + level);
			}
			
			outer.get().put(key, obj);
			
			return outer;
		}
		catch (Exception e){
			return outer;
		}
	}

	private static ItemStack deserializeItemStack(JSONObject json) {
		try {
			int quantity = (int) json.remove("amount");
			
			ItemStack item = ItemStack.builder().itemType(Sponge.getRegistry().getType(ItemType.class, json.remove("id").toString()).get()).quantity(quantity).build();
			
			if (json.has("display_name")){
				item.offer(Keys.DISPLAY_NAME, TextSerializers.JSON.deserialize(json.remove("display_name").toString()));
			}
			
			if (json.has("lore")){
				List<Text> lores =  new ArrayList<Text>();
				
				JSONArray array = (JSONArray) json.remove("lore");
				
				for (int i = 0; i < array.length(); i++){
					lores.add(TextSerializers.JSON.deserialize(array.getString(i)));
				}
				
				item.offer(Keys.ITEM_LORE, lores);
			}
			
			if (json.has("durability")){
				item.offer(Keys.ITEM_DURABILITY, (int) json.remove("durability"));
			}
			
			if (json.has("glowing")){
				item.offer(Keys.GLOWING, (boolean) json.remove("glowing"));
			}
			
			if (json.has("dye_color")){
				item.offer(Keys.DYE_COLOR, Sponge.getRegistry().getType(DyeColor.class, json.remove("dye_color").toString().toUpperCase()).get());
			}
			
			if (json.has("book_enchant")){
				String text = json.remove("book_enchant").toString();
				ItemEnchantment ie = new ItemEnchantment(Sponge.getRegistry().getType(Enchantment.class, text.split("#")[0].toUpperCase()).get(), Integer.valueOf(text.split("#")[1]));
				item.offer(Keys.STORED_ENCHANTMENTS, Arrays.asList(ie));
			}
			
			/*
			if (json.has("BRICK_TYPE")){
				item.offer(k, Sponge.getRegistry().getType(BrickType.class, json.remove(key).toString()).get());
			}
			else if (json.has("COAL_TYPE")){
				item.offer(k, Sponge.getRegistry().getType(CoalType.class, json.remove(key).toString()).get());
			}
			else if (json.has("COMPARATOR_TYPE")){
				item.offer(k, Sponge.getRegistry().getType(ComparatorType.class, json.remove(key).toString()).get());
			}
			else if (json.has("DIRT_TYPE")){
				item.offer(k, Sponge.getRegistry().getType(DirtType.class, json.remove(key).toString()).get());
			}
			else if (json.has("GOLDEN_APPLE_TYPE")){
				item.offer(k, Sponge.getRegistry().getType(GoldenApple.class, json.remove(key).toString()).get());
			}
			else if (json.has("PISTON_TYPE")){
				item.offer(k, Sponge.getRegistry().getType(PistonType.class, json.remove(key).toString()).get());
			}
			else if (json.has("FISH_TYPE")){
				item.offer(k, Sponge.getRegistry().getType(Fish.class, json.remove(key).toString()).get());
			}
			else if (json.has("SAND_TYPE")){
				item.offer(k, Sponge.getRegistry().getType(SandType.class, json.remove(key).toString()).get());
			}
			else if (json.has("WALL_TYPE")){
				item.offer(k, Sponge.getRegistry().getType(WallType.class, json.remove(key).toString()).get());
			}
			else if (json.has("SLAB_TYPE")){
				item.offer(k, Sponge.getRegistry().getType(SlabType.class, json.remove(key).toString()).get());
			}
			else if (json.has("TREE_TYPE")){
				item.offer(k, Sponge.getRegistry().getType(TreeType.class, json.remove(key).toString()).get());
			}
			else if (json.has("SANDSTONE_TYPE")){
				item.offer(k, Sponge.getRegistry().getType(SandstoneType.class, json.remove(key).toString()).get());
			}*/
			
			return item;
		} 
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void dropItem(Player player, ItemStack stack){
		Entity optItem = player.getLocation().getExtent().createEntity(EntityTypes.ITEM, player.getLocation().getPosition());
		
		if (optItem != null) {
		    Item item = (Item) optItem;
		    item.offer(Keys.REPRESENTED_ITEM, stack.createSnapshot());
		    player.getWorld().spawnEntity(item, Cause.builder().owner(player).build());
		}
	}
	
	public static EntityPixelmon getPlayerPokemon(Player player, int slot){
		try {
			Optional<PlayerStorage> optStorage = PixelmonStorage.pokeBallManager.getPlayerStorageFromUUID((MinecraftServer) Sponge.getServer(), player.getUniqueId());
			
			if (optStorage.isPresent()){
				PlayerStorage playerStorgae = optStorage.get();
				
				NBTTagCompound tag = playerStorgae.partyPokemon[slot];
				
				if (tag == null){ return null; }
				
				EntityPixelmon pokemon = (EntityPixelmon) PixelmonEntityList.createEntityFromNBT(tag, (net.minecraft.world.World) player.getWorld());
				
				return pokemon;
			}
			else {
				return null;
			}
		}
		catch (Exception e) {
			return null;
		}
	}
	
	public static int getPartySize(Player player){
		Optional<PlayerStorage> optStorage = PixelmonStorage.pokeBallManager.getPlayerStorageFromUUID((MinecraftServer) Sponge.getServer(), player.getUniqueId());
	
		if (optStorage.isPresent()){
			PlayerStorage playerStorgae = optStorage.get();
			return playerStorgae.partyPokemon.length;
		}
		else {
			return -1;
		}
	}
	
	public static JSONArray getPartyPokemon(Player player, String crit){
		JSONArray array = new JSONArray();
		
		Optional<PlayerStorage> optStorage = PixelmonStorage.pokeBallManager.getPlayerStorageFromUUID((MinecraftServer) Sponge.getServer(), player.getUniqueId());
	
		if (optStorage.isPresent()){
			PlayerStorage playerStorgae = optStorage.get();
			
			for (NBTTagCompound tag: playerStorgae.partyPokemon){
				JSONObject obj = new JSONObject();
				
				if (tag == null){ continue; }
				
				EntityPixelmon pokemon = (EntityPixelmon) PixelmonEntityList.createEntityFromNBT(tag, (net.minecraft.world.World) player.getWorld());

				try {
					//Save Pokemon Data
					if (crit.contains("lvl")){
						obj.put("lvl", pokemon.getLvl().getLevel());
					}
					if (crit.contains("type")){
						obj.put("type", pokemon.getPokemonName());
					}
					if (crit.contains("name")){
						obj.put("name", pokemon.getNickname());
					}
					if (crit.contains("nature")){
						obj.put("nature", pokemon.getNature());
					}
					if (crit.contains("ability")){
						obj.put("ability", pokemon.getAbility().getName());
					}
					if (crit.contains("growth")){
						obj.put("growth", pokemon.getGrowth().name());
					}
					if (crit.contains("gender")){
						obj.put("gender", pokemon.gender.name());
					}
					if (crit.contains("shiny")){
						obj.put("shiny", pokemon.getIsShiny());
					}
					if (crit.contains("moveset")){
						String m1 = pokemon.getMoveset().get(0) != null ? pokemon.getMoveset().get(0).baseAttack.getLocalizedName() : "Empty";
						String m2 = pokemon.getMoveset().get(1) != null ? pokemon.getMoveset().get(1).baseAttack.getLocalizedName() : "Empty";
						String m3 = pokemon.getMoveset().get(2) != null ? pokemon.getMoveset().get(2).baseAttack.getLocalizedName() : "Empty";
						String m4 = pokemon.getMoveset().get(3) != null ? pokemon.getMoveset().get(3).baseAttack.getLocalizedName() : "Empty";
						JSONObject moves = new JSONObject();
						moves.put("1", m1);
						moves.put("2", m2);
						moves.put("3", m3);
						moves.put("4", m4);
						
						obj.put("moveset", moves);
					}
					
					array.put(obj);
				}
				catch (Exception e){
					ChatUtili.sendPrefixedMessage(player, "&cFailed to save pokemon!");
				}
			}
			
			return array;
		}
		else {
			ChatUtili.sendPrefixedMessage(player, "&cYou pokemon cannot be found...");
		}
		
		return array;
	}
	
	public static int getNumberOfPokemon(Player player){
		JSONArray array = new JSONArray();
		
		Optional<PlayerStorage> optStorage = PixelmonStorage.pokeBallManager.getPlayerStorageFromUUID((MinecraftServer) Sponge.getServer(), player.getUniqueId());
	
		if (optStorage.isPresent()){
			PlayerStorage playerStorgae = optStorage.get();
			
			for (NBTTagCompound tag: playerStorgae.partyPokemon){
				JSONObject obj = new JSONObject();
				
				if (tag == null){ continue; }
				
				try {
					EntityPixelmon pokemon = (EntityPixelmon) PixelmonEntityList.createEntityFromNBT(tag, (net.minecraft.world.World) player.getWorld());

					obj.put("name", pokemon.getPokemonName());
					
					array.put(obj);
				}
				catch (Exception e){
					ChatUtili.sendPrefixedMessage(player, "&cFailed to save pokemon!");
					return 0;
				}
			}
			
			return array.length();
		}
		else {
			return 0;
		}
	}
	
	public static void printCurrency(Player source){
		if (economyExists()){
			ChatUtili.sendMessage(source, "&f----- &6Currency List &f-----");
			
			for (Currency c: economyService.getCurrencies()){
				ChatUtili.sendMessage(source, "&6" + c.getName());
			}
		}
		else {
			ChatUtili.sendPrefixedMessage(source, "&cNo economy found!");
		}
	}
	
	 public static String RomanNumerals(int Int) {
		    LinkedHashMap<String, Integer> roman_numerals = new LinkedHashMap<String, Integer>();
		    roman_numerals.put("M", 1000);
		    roman_numerals.put("CM", 900);
		    roman_numerals.put("D", 500);
		    roman_numerals.put("CD", 400);
		    roman_numerals.put("C", 100);
		    roman_numerals.put("XC", 90);
		    roman_numerals.put("L", 50);
		    roman_numerals.put("XL", 40);
		    roman_numerals.put("X", 10);
		    roman_numerals.put("IX", 9);
		    roman_numerals.put("V", 5);
		    roman_numerals.put("IV", 4);
		    roman_numerals.put("I", 1);
		    String res = "";
		    for(Map.Entry<String, Integer> entry : roman_numerals.entrySet()){
		      int matches = Int/entry.getValue();
		      res += repeat(entry.getKey(), matches);
		      Int = Int % entry.getValue();
		    }
		    return res;
		  }
	 
		  public static String repeat(String s, int n) {
		    if(s == null) {
		        return null;
		    }
		    final StringBuilder sb = new StringBuilder();
		    for(int i = 0; i < n; i++) {
		        sb.append(s);
		    }
		    return sb.toString();
		  }
		  
		  public static String subRndNumbers(String input){
			  final String REGEX = "(<(\\d+),(\\d+)>)";
			  
			  Pattern p = Pattern.compile(REGEX);
		         
	          // get a matcher object
	          Matcher m = p.matcher(input);
	         
	          String result = input;
	         
	          try {
	              while(m.find()) {
	                 String inputSub = m.group(1).toString().replaceAll("(>.*.<)", ">-<");
	                 
	                 for (String rep : inputSub.split("-")){
	                     result = result.replaceFirst(rep, String.valueOf(extractRndNumber(rep.trim())));
	                 }
	              }
	          }
	          catch (Exception e){
	        	  return result;
	          }

	          return result;
		  }
		  
		  public static Object extractRndNumber(String input){
		        try {
		            String data = input.substring(1, input.length() - 1);
		           
		            int start = Integer.valueOf(data.split(",")[0]);
		         
		            int end = Integer.valueOf(data.split(",")[1]);
		           
		            int result = new Random().nextInt((end + 1) - start) + start;
		           
		            return result;
		        }
		        catch (Exception e){
		            return input;
		        }
		    }
}

package me.koledogcodes.serversigns.utili;

import java.util.Timer;
import java.util.TimerTask;

import me.koledogcodes.serversigns.ServerSigns;

public class ServerSignTimer {
	
	public ServerSignTimer() {
		
	}
	
	public static void registerNewRepeatingTimer(TimerTask task, long delay, long period){
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(task, delay, period);
		ServerSigns.activeTimers.add(timer);
		ServerSigns.activeTasks.add(task);
	}
	
	public static Timer registerNewNonRepeatingTimer(TimerTask task, long delay_unti_execute){
		Timer timer = new Timer();
		timer.schedule(task, delay_unti_execute);
		ServerSigns.activeTimers.add(timer);
		ServerSigns.activeTasks.add(task);
		return timer;
	}
	
	public static void cancelAllTimers(boolean broadcast){
		if (broadcast){
			for (int i = 0; i < ServerSigns.activeTasks.size(); i++){
				ServerSigns.activeTasks.get(i).cancel();
				System.out.println("[ServerSigns] ServerSignsTask #" + i + " has been cancelled.");
			}
			
			for (int i = 0; i < ServerSigns.activeTimers.size(); i++){
				ServerSigns.activeTimers.get(i).cancel();
				System.out.println("[ServerSigns] ServerSignsTimer #" + i + " has been cancelled.");
			}
		}
		else {
			for (int i = 0; i < ServerSigns.activeTasks.size(); i++){
				ServerSigns.activeTasks.get(i).cancel();
			}
			
			for (int i = 0; i < ServerSigns.activeTimers.size(); i++){
				ServerSigns.activeTimers.get(i).cancel();
			}
		}
	}
}

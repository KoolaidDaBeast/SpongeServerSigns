package me.koledogcodes.serversigns.utili;

public enum SignAction {

	LIST,
	ADD,
	REMOVE,
	INSERT,
	EDIT,
	PRICE,
	EXP,
	COOLDOWN,
	GLOBAL_COOLDOWN,
	PLAYER_USES,
	USES,
	PERMISSION,
	GIVE_ITEM,
	TRADE_ITEM,
	TRADE_POKEMON,
	
	ENTITY_CHECK
}

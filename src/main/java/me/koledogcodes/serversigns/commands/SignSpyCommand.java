package me.koledogcodes.serversigns.commands;

import java.util.List;
import java.util.Optional;

import org.spongepowered.api.command.CommandCallable;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import me.koledogcodes.serversigns.utili.ChatUtili;
import me.koledogcodes.serversigns.utili.SignUtili;

public class SignSpyCommand implements CommandCallable {

	@Override
	public Optional<Text> getHelp(CommandSource arg0) {	
		return null;
	}

	@Override
	public Optional<Text> getShortDescription(CommandSource arg0) {
		return null;
	}

	@Override
	public List<String> getSuggestions(CommandSource arg0, String arg1, Location<World> arg2) throws CommandException {
		return null;
	}

	@Override
	public Text getUsage(CommandSource arg0) {
		return null;
	}

	@Override
	public CommandResult process(CommandSource source, String command) throws CommandException {
		if (!(source instanceof Player)){
			ChatUtili.sendMessage(source, "&cCommand can only be used ingame.");
			return CommandResult.success();
		}
		
		if (!source.hasPermission("svs.admin.*")){
			ChatUtili.sendPrefixedMessage(source, "&cYou do not have permission to use this command.");
			return CommandResult.success();
		}
		
		Player player = (Player) source;
		
		if (SignUtili.signSpyUsers.contains(player.getUniqueId())){
			SignUtili.signSpyUsers.remove(player.getUniqueId());
			SignUtili.getUserData(player.getUniqueId()).setValue("sign-spy", false);
			ChatUtili.sendPrefixedMessage(player, "&aYou have been removed from the sign spy list.");
		}
		else {
			SignUtili.signSpyUsers.add(player.getUniqueId());
			SignUtili.getUserData(player.getUniqueId()).setValue("sign-spy", true);
			ChatUtili.sendPrefixedMessage(player, "&aYou have been added to the sign spy list.");
		}
		
		return CommandResult.success();
	}

	@Override
	public boolean testPermission(CommandSource arg0) {
		return arg0.hasPermission("serversigns.admin.*");
	}

}

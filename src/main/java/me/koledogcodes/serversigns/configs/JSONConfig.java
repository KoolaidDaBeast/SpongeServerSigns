package me.koledogcodes.serversigns.configs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONConfig {

	private File file;
	final private static HashMap<String, String> cachedFile = new HashMap<String, String>();
	
	public JSONConfig(String path, String name){
		file = new File(path, name + ".json");
		
		generateFile();
		
		if (!cachedFile.containsKey(path + name)){
			cachedFile.put(path + name, readFile());
		}
	}
	
	private void generateFile(){
		if (!file.exists()){
			try {
				file.createNewFile();
				cachedFile.put(file.getPath() + file.getName(), "{}");
			} 
			catch (IOException e) {

			}
		}
	}
	
	@SuppressWarnings("resource")
	private String readFile(){
		try {
			FileReader fr = new FileReader(file);
			BufferedReader reader = new BufferedReader(fr);
			
			String text = "";
			
			String line;
			while ((line = reader.readLine()) != null){
				text += line;
			}
			
			return text;
		} 
		catch (Exception e) {
			return null;
		}
	}
	
	private String getCachedText(){
		if (!cachedFile.containsKey(file.getPath() + file.getName())){
			reloadFile();
		}
		
		return cachedFile.get(file.getPath() + file.getName());
	}
	
	private void write(){
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(file));
			writer.write(getCachedText());
			writer.close();
		} 
		catch (Exception e) {
			System.out.println("[ServerSigns] Failed to write to json file. '" + file.getName() + "'");
		}
	}
	
	public JSONObject parseJsonObject(String[] keys, Object[] values){
		JSONObject jsonObj = new JSONObject();
		
		try {
			for (int i = 0; i < keys.length; i++){
				jsonObj.put(keys[i], values[i]);
			}
		}
		catch (Exception e){
			return null;
		}
		
		return jsonObj;
	}
	
	public void reloadFile(){
		cachedFile.put(file.getPath() + file.getName(), readFile());
	}
	
	public static void reloadAllFile(){
		cachedFile.clear();
	}
	
	public void saveFile(){
		write();
	}
	
	public boolean checkKey(String key){
		if (getObject(key) == null){
			return false;
		}
		else {
			return true;
		}
	}
	
	public void generateValue(String key, Object value){
		if (!checkKey(key)){
			setValue(key, value);
		}
	}
	
	public void setValue(String key, Object value){
		try {
			JSONObject jsonObj = new JSONObject(getCachedText());
			jsonObj.put(key, value);
			cachedFile.put(file.getPath() + file.getName(), jsonObj.toString(4));
			
			write();
		} 
		catch (JSONException e) {
			return;
		}
	}
	
	public void removeKey(String key){
		try {
			JSONObject jsonObj = new JSONObject(getCachedText());
			jsonObj.remove(key);
			cachedFile.put(file.getPath() + file.getName(), jsonObj.toString(4));
			
			write();
		} 
		catch (JSONException e) {
			return;
		}
	}
	
	public JSONArray getJSONArray(String key){
		try {
			JSONObject jsonObj = new JSONObject(getCachedText());
			return jsonObj.getJSONArray(key);
		} 
		catch (JSONException e) {
			return null;
		}
	}
	
	public JSONObject getJSONObject(String key){
		try {
			JSONObject jsonObj = new JSONObject(getCachedText());
			return jsonObj.getJSONObject(key);
		} 
		catch (JSONException e) {
			return null;
		}
	}
	
	public boolean hasKey(String key){
		JSONObject jsonObj;
		try {
			jsonObj = new JSONObject(getCachedText());
			return jsonObj.has(key);
		} 
		catch (JSONException e) {
			return false;
		}
		
	}
	
	public Object getObject(String key){
		try {
			JSONObject jsonObj = new JSONObject(getCachedText());
			return jsonObj.get(key);
		} 
		catch (JSONException e) {
			return null;
		}
	}
	
	public String getString(String key){
		try {
			JSONObject jsonObj = new JSONObject(getCachedText());
			return jsonObj.getString(key);
		} 
		catch (JSONException e) {
			return null;
		}
	}
	
	public int getInteger(String key){
		try {
			JSONObject jsonObj = new JSONObject(getCachedText());
			return jsonObj.getInt(key);
		} 
		catch (JSONException e) {
			return 0;
		}
	}
	
	public boolean getBoolean(String key){
		try {
			JSONObject jsonObj = new JSONObject(getCachedText());
			return jsonObj.getBoolean(key);
		} 
		catch (JSONException e) {
			return false;
		}
	}
	
	public double getDouble(String key){
		try {
			JSONObject jsonObj = new JSONObject(getCachedText());
			return jsonObj.getDouble(key);
		} 
		catch (JSONException e) {
			return 0;
		}
	}
	
	public Long getLong(String key){
		try {
			JSONObject jsonObj = new JSONObject(getCachedText());
			return Long.valueOf(jsonObj.get(key).toString());
		} 
		catch (JSONException e) {
			return Math.round(getDouble(key));
		}
	}
	
	public Object[] getArray(String key){
		try {
			JSONObject jsonObj = new JSONObject(getCachedText());
			
			return jsonObj.get(key).toString().substring(2, jsonObj.get(key).toString().length() - 2).split("\",\"");
		} 
		catch (Exception e) {
			return new Object[]{};
		}
	}
	
	public List<Object> getListArray(String key){
		try {
			return new ArrayList<Object>(Arrays.asList(getArray(key)));
		} 
		catch (Exception e) {
			return new ArrayList<Object>();
		}
	}
	
	public void clearCache(){
		cachedFile.remove(file.getPath() + file.getName());
	}
	
	public void deleteConfig(){
		clearCache();
		file.delete();
	}
}

package me.koledogcodes.serversigns.configs;

import java.io.File;

public class FileConfiguration {
	
	private static JSONConfig jsonConfig = null;

	public static void generateMainConfig(){
		jsonConfig = new JSONConfig("config/ServerSigns", "config");
		jsonConfig.generateValue("prefix", "&f[&4&lS&c&lV&4&lS&f]");
		jsonConfig.generateValue("accepted-blocks", "wall_sign,standing_sign");
		jsonConfig.generateValue("color-signs", true);
	}
	
	public static JSONConfig getMainConfig(){
		return jsonConfig;
	}
	
	public static void generateFolder(String path){
		final File f = new File(path);
		
		if (!f.exists()){
			f.mkdirs();
		}
	}
}

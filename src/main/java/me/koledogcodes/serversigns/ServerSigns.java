package me.koledogcodes.serversigns;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GamePostInitializationEvent;
import org.spongepowered.api.event.game.state.GameStartingServerEvent;
import org.spongepowered.api.plugin.Dependency;
import org.spongepowered.api.plugin.Plugin;

import me.koledogcodes.serversigns.commands.ServerSignsCommand;
import me.koledogcodes.serversigns.commands.SignSpyCommand;
import me.koledogcodes.serversigns.configs.FileConfiguration;
import me.koledogcodes.serversigns.events.BasicSignListener;
import me.koledogcodes.serversigns.utili.SignUtili;

@Plugin(id = "me.koledogcodes.serversigns", name = "ServerSigns", version = "1.0.0",
		dependencies = @Dependency(id = "economylite", optional = true))

public class ServerSigns {
	
	public static List<Timer> activeTimers = new ArrayList<Timer>();
	public static List<TimerTask> activeTasks = new ArrayList<TimerTask>();

	@Listener
	public void onInitialization(GameStartingServerEvent event){
		//TODO CONFIG
		FileConfiguration.generateFolder("config/ServerSigns");
		FileConfiguration.generateFolder("config/ServerSigns/signs");
		FileConfiguration.generateFolder("config/ServerSigns/users");
		FileConfiguration.generateMainConfig();
		
		//TODO COMMANDS
		Sponge.getCommandManager().register(this, new ServerSignsCommand(), "svs");
		Sponge.getCommandManager().register(this, new SignSpyCommand(), "signspy");
		
		//TODO EVENTS
		Sponge.getEventManager().registerListeners(this, new BasicSignListener());
	}
	
	@Listener
	public void postInit(GamePostInitializationEvent event) {
		SignUtili.setupEconomy(); 
	}
}
